<?xml version="1.0"?>
<!DOCTYPE rfc SYSTEM "rfc2629.dtd">

<!DOCTYPE rfc SYSTEM "rfc2629.dtd" [
]>

<?rfc compact="no"?>
<?rfc toc="yes"?>
<?rfc symrefs="yes"?>

<rfc category="info" ipr="trust200902"
     docName="draft-josefsson-salsa20-00">

  <front>

    <title abbrev="salsa20">
      The Salsa20 stream cipher
    </title>

    <author initials="S." surname="Josefsson"
            fullname="Simon Josefsson">
      <organization>SJD AB</organization>
      <address>
        <email>simon@josefsson.org</email>
        <uri>http://josefsson.org/</uri>
      </address>
    </author>

    <author initials="J." surname="Strombergson"
            fullname="Joachim Strombergson">
      <organization>Secworks Sweden AB</organization>
      <address>
        <email>joachim@secworks.se</email>
        <uri>http://secworks.se/</uri>
      </address>
    </author>

    <date month="November" year="2012"/>

    <abstract>

      <t>This document describe the Salsa20 stream cipher and the
      Salsa20 core hash function.  The document provides code as well
      as test vectors.</t>

    </abstract>
    
  </front>

  <middle>

    <section anchor="intro"
             title="Introduction">

      <t>Salsa20 is stream cipher algorithm that has been designed for
      high performance in software implementations. The cipher has
      compact implementation and uses few resources and inexpensive
      operations that makes it suitable for implementation on a wide
      range of architectures. The cipher has been designed to minimize
      risk due to side channel attacks.</t>

      <t>The Salsa20 cipher has short initialization sequence and
      provides good key ability and good performance over a range of
      data sizes.</t>

      <t>Salsa20 is one of the ciphers selected as part of the eSTREAM
      portfolio of stream ciphers.</t>
    </section>

    <section anchor="Salsa20"
	     title="The Salsa20 Core Function">

      <t>Salsa20 is a hash function from 64-octet strings to 64-octet
      strings.  Note that Salsa20/8 Core is not a cryptographic hash
      function since it is not collision-resistant.  See section 8 of
      <xref target="SALSA20SPEC" /> for its specification, and <xref
      target="SALSA20CORE" /> for more information.</t>

    </section>

    <section anchor="coretestvectors"
             title="Test Vectors for the Salsa20/8 Core">

      <t>Below is a sequence of octets to illustrate input and output
      values for the Salsa20 Core with 8 rounds.  The octets are hex
      encoded and whitespace is inserted for readability.</t>

      <figure>
	<artwork><![CDATA[
INPUT:
7e 87 9a 21 4f 3e c9 86 7c a9 40 e6 41 71 8f 26
ba ee 55 5b 8c 61 c1 b5 0d f8 46 11 6d cd 3b 1d
ee 24 f3 19 df 9b 3d 85 14 12 1e 4b 5a c5 aa 32
76 02 1d 29 09 c7 48 29 ed eb c6 8d b8 b8 c2 5e

OUTPUT:
a4 1f 85 9c 66 08 cc 99 3b 81 ca cb 02 0c ef 05
04 4b 21 81 a2 fd 33 7d fd 7b 1c 63 96 68 2f 29
b4 39 31 68 e3 c9 e6 bc fe 6b c5 b7 a0 6d 96 ba
e4 24 cc 10 2c 91 74 5c 24 ad 67 3d c7 61 8f 81
]]></artwork>
      </figure>

    </section>

    <section anchor="core-test-vectors"
             title="Test Vectors for the Salsa20 Cipher Suites">

      <t>Below is a number of Known Answer Testvectors for the different
      Salsa20 Cipher Suites defined. The test vectors are hex
      encoded and whitespace is inserted for readability.</t>
      
      <t>
      The hex sequence for the second KAT for each Cipher Suite is
      generated by hashing the sentence 'All Your Base Are Belong To Us'
      with the SHA-256 hash function. For 128 bit keys only the first
      128 bits are used.
      echo -n "All Your Base Are Belong To Us" | openssl dgst -sha256
      6ac3a9027f437970fef8025ec487713e51d41690f0c8b041ad11c04f57adf5a3
      </t>

      <figure>
	<artwork><![CDATA[

CIPHER: SALSA208_128
KEY: 0x55555555555555555555555555555555
IV: 0x0102030405060708

OUTPUT:
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
]]></artwork>
      </figure>


      <figure>
	<artwork><![CDATA[

CIPHER: SALSA208_128
KEY: 0x6ac3a9027f437970fef8025ec487713e
IV: 0x0101010101010101

OUTPUT:
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
]]></artwork>
      </figure>

      <figure>
	<artwork><![CDATA[

CIPHER: SALSA2012_128
KEY: 0x55555555555555555555555555555555
IV: 0x0102030405060708

OUTPUT:
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
]]></artwork>
      </figure>


      <figure>
	<artwork><![CDATA[

CIPHER: SALSA2012_128
KEY: 0x6ac3a9027f437970fef8025ec487713e
IV: 0x0101010101010101

OUTPUT:
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
]]></artwork>
      </figure>

      <figure>
	<artwork><![CDATA[

CIPHER: SALSA2012_256
KEY: 0x5555555555555555555555555555555555555555555555555555555555555555
IV: 0x0102030405060708

OUTPUT:
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
]]></artwork>
      </figure>


      <figure>
	<artwork><![CDATA[

CIPHER: SALSA2012_256
KEY: 0x6ac3a9027f437970fef8025ec487713e51d41690f0c8b041ad11c04f57adf5a3
IV: 0x0101010101010101
OUTPUT:
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
]]></artwork>
      </figure>

      <figure>
	<artwork><![CDATA[

CIPHER: SALSA2020_256
KEY: 0x5555555555555555555555555555555555555555555555555555555555555555
IV: 0x0102030405060708

OUTPUT:
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
]]></artwork>
      </figure>


      <figure>
	<artwork><![CDATA[

CIPHER: SALSA2020_256
KEY: 0x6ac3a9027f437970fef8025ec487713e51d41690f0c8b041ad11c04f57adf5a3
IV: 0x0101010101010101

OUTPUT:
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa
]]></artwork>
      </figure>

    </section>

    <section anchor="copying-conditions"
             title="Copying Conditions">

      <t>The authors agree to grant third parties the irrevocable
      right to copy, use and distribute this entire document or any
      portion of it, with or without modification, in any medium,
      without royalty, provided that, unless separate permission is
      granted, redistributed modified works do not contain misleading
      author, version, name of work, or endorsement information.</t>

    </section>

    <section anchor="ack"
             title="Acknowledgements">

      <t>To Be Written. DJB DJB DJB.</t>

    </section>

    <section anchor="security"
             title="Security Considerations">

      <t>To Be Written.</t>

    </section>

  </middle>

  <back>

    <references title="Normative References">

      <reference anchor="SALSA20SPEC">
	<front>
	  <title>Salsa20 specification</title>
	  <author initials="D.J." surname="Bernstein"
		  fullname="D.J. Bernstein"/>
	  <date month="April" year="2005" />
	</front>
	<seriesInfo name="WWW" value="http://cr.yp.to/snuffle/spec.pdf" />
      </reference>

      <reference anchor="SALSA20CORE">
	<front>
	  <title>The Salsa20 Core</title>
	  <author initials="D.J." surname="Bernstein"
		  fullname="D.J. Bernstein"/>
	  <date month="March" year="2005" />
	</front>
	<seriesInfo name="WWW" value="http://cr.yp.to/salsa20.html" />
      </reference>

    </references>

  </back>

</rfc>
