all: draft-josefsson-salsa20.txt draft-josefsson-salsa20.html draft-josefsson-salsa20-tls.txt draft-josefsson-salsa20-tls.html draft-josefsson-salsa20-tls-from--03.diff.html

clean:
	rm -f *~ \
	draft-josefsson-salsa20.txt draft-josefsson-salsa20.html \
	draft-josefsson-salsa20-tls.txt draft-josefsson-salsa20-tls.html

draft-josefsson-salsa20.txt: draft-josefsson-salsa20.xml
	xml2rfc $<

draft-josefsson-salsa20.html: draft-josefsson-salsa20.xml
	xml2rfc $< $@

draft-josefsson-salsa20-tls.txt: draft-josefsson-salsa20-tls.xml
	xml2rfc $<

draft-josefsson-salsa20-tls.html: draft-josefsson-salsa20-tls.xml
	xml2rfc $< $@

draft-josefsson-salsa20-tls-from--03.diff.html: draft-josefsson-salsa20-tls.txt
	rfcdiff draft-josefsson-salsa20-tls-03.txt draft-josefsson-salsa20-tls.txt 
